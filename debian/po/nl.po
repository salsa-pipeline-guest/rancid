# Dutch translation of rancid debconf templates.
# Copyright (C) 2012 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the rancid package.
# Jeroen Schot <schot@a-eskwadraat.nl>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: rancid 2.3.6-2\n"
"Report-Msgid-Bugs-To: rancid@packages.debian.org\n"
"POT-Creation-Date: 2009-02-15 21:21+0100\n"
"PO-Revision-Date: 2012-02-28 10:04+0100\n"
"Last-Translator: Jeroen Schot <schot@a-eskwadraat.nl>\n"
"Language-Team: Debian l10n Dutch <debian-l10n-dutch@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: note
#. Description
#: ../rancid.templates:1001
msgid "Note"
msgstr "Opmerking"

#. Type: note
#. Description
#: ../rancid.templates:1001
msgid ""
"Rancid debian package is still in an alpha stage.  If you find problems "
"first check all the docs and then report them as bugs as soon as possible.  "
"Currently it doesn't have any installation script to help you configuring "
"it, look at the examples directory under /usr/share/doc/rancid to look for "
"some example for your configuration."
msgstr ""
"Het Debianpakket van rancid bevindt zich nog in een alfa-stadium. Als u "
"problemen vindt, raadpleeg dan eerst de documentatie en meld ze zo snel "
"mogelijk als bugs. Er is op dit moment geen installatiescript om u te helpen "
"met de configuratie, bekijk de voorbeeldenmap onder /usr/share/doc/rancid "
"voor configuratievoorbeelden."

#. Type: boolean
#. Description
#: ../rancid.templates:2001
msgid "Really continue?"
msgstr "Echt verdergaan?"

#. Type: boolean
#. Description
#: ../rancid.templates:2001
msgid ""
"Please check, whether you made a backup copy of your rancid data.  If it's "
"your first installation of rancid accept here, otherwise decline, perform "
"the backup and then run \"dpkg-reconfigure rancid\""
msgstr ""
"Controleer eerst of u een reservekopie van uw rancid-gegevens heeft gemaakt. "
"Als dit uw eerste installatie van rancid is, ga dan verder. Anders dient u "
"te stoppen, een reservekopie te maken en \"dpkg-reconfigure rancid\" uit te "
"voeren."
